'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
// import urlParse from 'url-parse';


exports.generateClientHeader = generateClientHeader;

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

var _crypto = require('./crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function generateClientHeader(uri, method, options) {

  var result = {
    field: '',
    artifacts: {},
    err: ''
  };

  // Validate inputs

  if (!uri || typeof uri !== 'string' && (typeof uri === 'undefined' ? 'undefined' : _typeof(uri)) !== 'object' || !method || typeof method !== 'string' || !options || (typeof options === 'undefined' ? 'undefined' : _typeof(options)) !== 'object') {

    result.err = 'Invalid argument type';
    return result;
  }

  // Application time

  var timestamp = options.timestamp || _utils2.default.nowSecs();

  // Validate credentials

  var credentials = options.credentials;
  if (!credentials || !credentials.id || !credentials.key || !credentials.algorithm) {

    result.err = 'Invalid credential object';
    return result;
  }

  if (_crypto2.default.algorithms.indexOf(credentials.algorithm) === -1) {
    result.err = 'Unknown algorithm';
    return result;
  }

  // Parse URI
  var parsed = void 0;
  if (typeof uri === 'string') {
    parsed = _url2.default.parse(uri);
  }

  // Calculate signature

  var artifacts = {
    ts: timestamp,
    nonce: options.nonce || _crypto2.default.randomString(6),
    method: method,
    resource: parsed.pathname + (parsed.search || ''), // Maintain trailing '?'
    host: parsed.hostname,
    port: parsed.port || (parsed.protocol === 'http:' ? 80 : 443),
    hash: options.hash,
    ext: options.ext,
    app: options.app,
    dlg: options.dlg
  };

  result.artifacts = artifacts;

  // Calculate payload hash

  if (!artifacts.hash && (options.payload || options.payload === '')) {

    artifacts.hash = _crypto2.default.calculatePayloadHash(options.payload, credentials.algorithm, options.contentType);
  }

  var mac = _crypto2.default.calculateMac('header', credentials, artifacts);

  // Construct header

  var hasExt = artifacts.ext !== null && artifacts.ext !== undefined && artifacts.ext !== ''; // Other falsey values allowed
  var header = 'Hawk id="' + credentials.id + '", ts="' + artifacts.ts + '", nonce="' + artifacts.nonce + (artifacts.hash ? '", hash="' + artifacts.hash : '') + (hasExt ? '", ext="' + _utils2.default.escapeHeaderAttribute(artifacts.ext) : '') + '", mac="' + mac + '"';
  if (artifacts.app) {
    header = header + ', app="' + artifacts.app + (artifacts.dlg ? '", dlg="' + artifacts.dlg : '') + '"';
  }

  result.field = header;

  return result;
}