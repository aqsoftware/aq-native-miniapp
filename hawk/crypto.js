'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _reactNativeCrypto = require('react-native-crypto');

var _reactNativeCrypto2 = _interopRequireDefault(_reactNativeCrypto);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cryptoez = function () {
  function Cryptoez() {
    _classCallCheck(this, Cryptoez);
  }

  _createClass(Cryptoez, null, [{
    key: 'randomString',
    value: function randomString(size) {

      var buffer = this.randomBits((size + 1) * 6);
      if (buffer instanceof Error) {
        return buffer;
      }

      var string = buffer.toString('base64').replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, '');
      return string.slice(0, size);
    }
  }, {
    key: 'randomBits',
    value: function randomBits(bits) {
      if (!bits || bits < 0) {

        throw new Error('Invalid random bits count');
      }

      var bytes = Math.ceil(bits / 8);
      try {
        return _reactNativeCrypto2.default.randomBytes(bytes);
      } catch (err) {
        throw new Error('Failed generating random bits: ' + err.message);
      }
    }
  }, {
    key: 'calculateMac',
    value: function calculateMac(type, credentials, options) {

      var normalized = this.generateNormalizedString(type, options);
      var hmac = _reactNativeCrypto2.default.createHmac(credentials.algorithm, credentials.key).update(normalized);
      var digest = hmac.digest('base64');
      return digest;
    }
  }, {
    key: 'generateNormalizedString',
    value: function generateNormalizedString(type, options) {

      var resource = options.resource || '';
      if (resource && resource[0] !== '/') {

        var url = _url2.default.parse(resource, false);
        resource = url.path; // Includes query
      }

      var normalized = 'hawk.' + Cryptoez.headerVersion + '.' + type + '\n' + options.ts + '\n' + options.nonce + '\n' + (options.method || '').toUpperCase() + '\n' + resource + '\n' + options.host.toLowerCase() + '\n' + options.port + '\n' + (options.hash || '') + '\n';

      if (options.ext) {
        normalized = normalized + options.ext.replace('\\', '\\\\').replace('\n', '\\n');
      }

      normalized = normalized + '\n';

      if (options.app) {
        normalized = normalized + options.app + '\n' + (options.dlg || '') + '\n';
      }

      return normalized;
    }
  }, {
    key: 'calculatePayloadHash',
    value: function calculatePayloadHash(payload, algorithm, contentType) {

      var hash = this.initializePayloadHash(algorithm, contentType);
      hash.update(payload || '');
      return this.finalizePayloadHash(hash);
    }
  }, {
    key: 'initializePayloadHash',
    value: function initializePayloadHash(algorithm, contentType) {

      var hash = _reactNativeCrypto2.default.createHash(algorithm);
      hash.update('hawk.' + Cryptoez.headerVersion + '.payload\n');
      hash.update(Utils.parseContentType(contentType) + '\n');
      return hash;
    }
  }, {
    key: 'finalizePayloadHash',
    value: function finalizePayloadHash(hash) {
      hash.update('\n');
      return hash.digest('base64');
    }
  }]);

  return Cryptoez;
}();

Cryptoez.headerVersion = '1';
Cryptoez.algorithms = ['sha1', 'sha256'];
exports.default = Cryptoez;