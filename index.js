'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultUIBridge = exports.defaultLifeCycle = undefined;

var _MediaStorage = require('./MediaStorage');

Object.keys(_MediaStorage).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _MediaStorage[key];
    }
  });
});

var _reactNative = require('react-native');

var _LifeCycle = require('./LifeCycle');

var _UIBridge = require('./UIBridge');

// export const defaultLifeCycle = NativeModules.RNMiniAppLifeCycleBridge;
// export const defaultUIBridge = NativeModules.RNMiniAppUIBridge;

// import './shim.js';
var defaultLifeCycle = _reactNative.NativeModules.MiniAppLifeCycleBridge;

// export * from './CloudStorage';

if (defaultLifeCycle === undefined) {
  exports.defaultLifeCycle = defaultLifeCycle = new _LifeCycle.LifeCycle();
}
exports.defaultLifeCycle = defaultLifeCycle;


var defaultUIBridge = _reactNative.NativeModules.MiniAppUIBridge;
if (defaultUIBridge === undefined) {
  exports.defaultUIBridge = defaultUIBridge = new _UIBridge.UIBridge();
}
exports.defaultUIBridge = defaultUIBridge;