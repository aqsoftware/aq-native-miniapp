'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
Stub class that emulates AQ MiniApp UIBridge API

Copyright (c) 2017 AQ Software Inc.
*/

var UIBridge = exports.UIBridge = function () {
  function UIBridge() {
    _classCallCheck(this, UIBridge);
  }

  _createClass(UIBridge, [{
    key: 'showTitleInput',


    /**
    Requests the AQ App to show a text input UI for the user to input a title
     @param {function(value: string): void} callback - Callback function to be called when
      a title has been input by the user
    */
    value: function showTitleInput(callback) {
      callback('Lorem Ipsum');
    }

    /**
    Requests the AQ App to show a selector UI from a list of image web urls
     @param {string} key - Unique key identifying this particular Requests
    @param {string} title - Title to be shown to the selector UI
    @param {string[]} imageUrls - An array of urls pointing to images that will be
      shown by the selector UI
    @param {function(key: string, value: Object): void} callback - Callback function to be called when
      an image is selected from imageUrls
    */

  }, {
    key: 'showWebImageSelector',
    value: function showWebImageSelector(key, title, imageUrls, callback) {
      callback(key, 'http://lorempixel.com/320/568/');
    }

    /**
    Requests the AQ App to show a selector UI showing a list of available gallery images
     @param {string} key - Unique key identifying this particular Requests
    @param {string} title - Title to be shown to the selector UI
    @param {function(key: string, value: Object): void} callback - Callback function to be called when
      an image is selected
    */

  }, {
    key: 'showGalleryImageSelector',
    value: function showGalleryImageSelector(key, title, callback) {
      callback(key, 'http://lorempixel.com/320/568/');
    }

    /**
    Requests the AQ App to show a selector UI showing a list of friends
     @param {string} key - Unique key identifying this particular Requests
    @param {function(key: string, value: Object[]): void} callback - Callback function to be called when
      a list of friends has been selected
    */

  }, {
    key: 'showFriendsSelector',
    value: function showFriendsSelector(key, callback) {
      callback(key, [{
        id: '7ywNEFe4EeeVakrmJEcW4w',
        displayName: 'John Doe',
        avatarBig: 'http://lorempixel.com/120/120/',
        avatarSmall: 'http://lorempixel.com/48/48/'
      }, {
        id: '86U5IFe4EeeVakrmJEcW4w',
        displayName: 'John Smith',
        avatarBig: 'http://lorempixel.com/120/120/',
        avatarSmall: 'http://lorempixel.com/48/48/'
      }]);
    }
  }]);

  return UIBridge;
}();