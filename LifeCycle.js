'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LifeCycle = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _base64Js = require('base64-js');

var _base64Js2 = _interopRequireDefault(_base64Js);

var _v = require('uuid/v1');

var _v2 = _interopRequireDefault(_v);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
Stub class that generates stub functionality for AQ MiniApp's LifeCycle API

Copyright (c) 2017 AQ Software Inc.
*/
var LifeCycle = exports.LifeCycle = function () {
  function LifeCycle() {
    _classCallCheck(this, LifeCycle);
  }

  _createClass(LifeCycle, [{
    key: 'setOnDataCallback',


    /**
    Sets the callback function to be called when the AQ App sets data
    that will be used by the miniapp.
     @param {function(value: Object): void} callback - Callback function to call
      when data is available from the AQ App
    */
    value: function setOnDataCallback(callback) {}

    /**
    Sets the callback function to be called when the AQ App requests the create screen
    to provide it with the current item data. This data will then be passed by the AQ App
    to the join screen
     @param {function(): void} callback - Callback function to call
      when the AQ App requests the current item data
    */

  }, {
    key: 'setRequestPreviewCallback',
    value: function setRequestPreviewCallback(callback) {}

    /**
    Instructs the mini app to save the current data produced by the create screens,
    to some persistent storage, usually a cloud storage. The AQ App provides an ID
    that will be associated with this data, which will be used at a later time when
    this same ID is provided to the join screen.
     Once the AQ App calls this function, you need to inform it whether publishing
    succeeded or not using the
     @param {function(id: string): void} callback - Callback function to call
      when the AQ App requests the the miniapp to publish the data
    */

  }, {
    key: 'setPublishCallback',
    value: function setPublishCallback(callback) {}

    /**
    Requests the AQ App to show the preview screen given some data.
     This function will trigger the AQ App to show the preview dialogue of the mini-app,
    eventually passing the given parameters as data for the preview.
     If any of the parameters, except data, is null, the preview screen will not be shown.
     @param {string} title - Title obtained from user through showTitleInput()
    @param {string} coverImageUrl - Cover image obtained from user. This can be a data-uri image,
      or normal web url and must be a 640x1136 JPEG image.
    @param {Object} data - Any mini-app specific data.
    */

  }, {
    key: 'showPreviewWithData',
    value: function showPreviewWithData(title, coverImageUrl, data) {}

    /**
    Generates a unique URL-safe Base64-encoded Id
    */

  }, {
    key: 'generateId',
    value: function generateId() {
      var arr = new Array(16);
      (0, _v2.default)(null, arr, 0);
      return _base64Js2.default.fromByteArray(arr).replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
    }

    /**
    Provides the AQ App join data for processing
     @param {string} id - Optional unique URL-safe UUID that will be used by the AQ app
      to reference this a particular join.
    @param {string} joinImageUrl - An image representing the output of the join screen.
      Join output image must a 640x1136 JPEG image and not a data-uri. If image obtained came from
      the phone's gallery, you need to upload it using CloudStorage.uploadMedia() api,
      to produce a valid url for the image.
    @param {string} winCriteriaPassed - Boolean value indicating whether this particular join
      resulted in a win or lose
    @param {Object} notificationItem - Object containing information to create notifications for
      users
    */

  }, {
    key: 'join',
    value: function join(id, joinImageUrl, winCriteriaPassed, notificationItem) {}

    /**
    Ends the join screen, providing the AQ App with a caption and a join output image.
    */

  }, {
    key: 'end',
    value: function end() {}

    /**
    Informs the AQ App that publishing succeeded.
     */

  }, {
    key: 'publishSucceded',
    value: function publishSucceded() {}

    /**
    Informs the AQ App that publishing failed
    */

  }, {
    key: 'publishFailed',
    value: function publishFailed() {}
  }]);

  return LifeCycle;
}();